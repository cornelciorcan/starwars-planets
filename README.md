This is a sample application that shows the planets of Star Wars using the API endpoint http://swapi.co/api/planets/
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app) and has not yet been [ejected](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#npm-run-eject).

CSS files are not included in the repository (apart from the one in the build folder) because they are generated by a [sass watcher](https://github.com/michaelwayman/node-sass-chokidar) during the development. 

# How to run the application

To run the application in a unix system: 
* download and extract a compressed archive or clone this gitlab repository
* using the command line enter the project folder with `cd <path-to-project>/starwars-planet`
* download the required libraries with `yarn install` or `npm install` 
* execute the application with `yarn start` or `npm run start`
* the application will start on your default browser. In case it doesn't, digit `localhost:3000` on your browser url bar 

Alternatively you could copy or link the build folder on a web server like [Apache](https://httpd.apache.org/) and run it on the browser. 

# How to run automated tests

Unit tests can be executed with `yarn test` or `npm run test`
An HTML test coverage report can be generated using `yarn react-scripts test --env=jsdom --coverage` or `npm run react-scripts test --env=jsdom --coverage`

# Design notes 

The focus of this project was to design elegant, reusable and testable code over efficiency. 
Due to the kind of data to show - ten records at once, each with many short fields - a table looked like the best choice. 
A requisite was to build a mobile friendly application, which can be hard for an application showing a large table. Unfortunately many powerful react table tool kits don't implement truly responsive tables. So I chose [react-super-responsive-table](https://www.npmjs.com/package/react-super-responsive-table) that is widely used and recently maintained. It doesn't provide any fancy feature, but it's light weight, easy to use and not opinionated, thus easy to extend. Global result sorting wasn't required so I implemented by myself a sorted version of the table. 
I used another simple library ([react-paginate](https://github.com/AdeleD/react-paginate)) to show the pagination toolbar. 
I tried to keep the components as short and decoupled as possible: 
* `Planets` is the only component responsible for calling the API;
* `SortedTable` extends (and reuses) `Table` and is responsible of sorting the data;
* `Table` encapsulates `react-super-responsive-table` to make it easy to replace it with another library. The same stands for `Pagination` that encapsulates `react-paginate`;
* `TableAdapter` is used by `Planets` to configure `Table` headers and convert raw data to rows.

To avoid calling the [Film API](https://swapi.co/api/films) multiple times with the same parameters I implemented a basic cache using the redux store

`React-create-app` comes with [jest](https://jestjs.io/) included. I added [enzyme](https://github.com/airbnb/enzyme) to mount components and I made tests to cover most of the logic and store functions, leaving behind the purely presentation functions. 


