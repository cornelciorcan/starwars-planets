/**
 * Convert object to query string
 * @param {object} params
 * @returns {string}
 * @throws {Error}
 */
export const objectToQueryString = params =>
    Object.entries(params)
        .map(([key, value]) => {
            if (value && typeof value === 'object') {
                throw new Error ('Cannot convert objects to query string values')
            }
            return `${encodeURIComponent(key)}=${encodeURIComponent(value)}`
        })
        .join('&')

/**
 * Check if string s is a valid number
 * @param {string} s
 * @returns {bool}
 */
export const isNumberString = (s) => {
    if (typeof s !== 'string') {
        throw new Error('Expected string')
    }
    return !!s.match(/^(-?\d+\.\d+)$|^(-?\d+)$/)
}

/**
 * Compare values
 * @param {*} a
 * @param {*} b
 * @returns {number} -1|0|1
 */
export const compare = (a, b) => a == b ? 0 : (a > b ? 1 : -1) // eslint-disable-line no-use-before-define