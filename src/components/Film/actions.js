export const ADD_FILM = 'ADD_FILM'

export const addFilm = (id, film) => ({
    type: ADD_FILM,
    id,
    film
})