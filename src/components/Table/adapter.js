export default class TableAdapter {

    /**
	 * Get headers as map {name: data}
	 * @return {object}
	 */
    getHeaders() {
        return {}
    }

    /**
     * @param {array} data
     * @return {array}
     */
    getRows(data) {
        return []
    }

}