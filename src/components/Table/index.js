import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table, Thead, Tbody, Tr, Th, Td } from 'react-super-responsive-table'
import './index.css'
import TableAdapter from './adapter'


class ResponsiveTable extends Component {

    renderHeaders () {
        return Object.entries(this.props.adapter.getHeaders())
            .map(([key, value]) => this.renderHeader(key, value.label, value.sortable))
    }

    renderHeader(name, label, sortable) {
        return <Th key={name}>
            <span>{label}</span>
        </Th>
    }

    renderRows (data, adapter, keyGetter) {
        return adapter.getRows(data).map(row => 
            <Tr key={keyGetter(row)}>
                {this.renderRowItems(row)}
            </Tr>
        )
    }
    
    renderRowItems (row) {
        return Object.keys(this.props.adapter.getHeaders())
            .map(key => this.renderRowItem(key, row[key]))
    }

    renderRowItem (key, value) {
        return (
            <Td key={key}>
                <div>{value}</div>
            </Td>
        )
    }

    render() {
        let {data, adapter, keyGetter} = this.props
        return (
            <Table className="table table-striped table-sm">
                <Thead>
                    <Tr>
                        {this.renderHeaders()}
                    </Tr>
                </Thead>
                <Tbody>
                    {this.renderRows(data, adapter, keyGetter)}
                </Tbody>
            </Table>
        )
    }
}

ResponsiveTable.propTypes = {
    data: PropTypes.array.isRequired,
    adapter: PropTypes.instanceOf(TableAdapter).isRequired,
    keyGetter: PropTypes.func.isRequired
}

ResponsiveTable.defaultProps = {

}

export default ResponsiveTable
