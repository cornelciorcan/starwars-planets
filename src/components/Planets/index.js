import React, { Component } from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import 'whatwg-fetch'
import 'promise'
import Paginate from '../Paginate/bootstrap'
import SortedTable from '../Table/sorted'
import {objectToQueryString} from '../../utils/index'
import {addFilm} from '../Film/actions'
import {getFilmId} from '../Film/index'
import TableAdapter from './table-adapter'

export class Planets extends Component {

    constructor() {
        super()

        this.state = {
            items:     [],
            pageCount: 0,
            current:   0,
            search:    ''
        }

        this.tableAdapter = new TableAdapter()
        this.loadPlanets = this.loadPlanets.bind(this)
        this.handlePageClick = this.handlePageClick.bind(this)
        this.handleOnSearch = this.handleOnSearch.bind(this)
    }

    keyGetter = (row) => row.name

    /**
     *
     * @param {array} planets
     * @returns {Set<string>}
     */
    getFilmUrlSet(planets) {
        let filmUrls = new Set();
        planets.forEach(planet => {
            planet.films.forEach(film => {
                filmUrls.add(film)
            })
        })
        return filmUrls
    }

    loadPlanets () {
        //console.log('loadPlanets', this.state.current)
        let params = {
            page: this.state.current + 1
        }
        if (this.state.search.length > 1) {
            params.search = this.state.search
        }
        fetch('https://swapi.co/api/planets/?' + objectToQueryString(params))
            .then(res => res.json())
            .then(json => {
                let {count, results} = json

                this.loadFilms(this.getFilmUrlSet(results))
                this.setState({
                    items:      results,
                    pageCount : Math.ceil(count / this.props.perPage)
                })
            })
            .catch(err => console.error(err))
    }

    /**
     *
     * @param {Set<string>} filmUrls
     * @returns {undefined|PromiseLike<T>|Promise<T>|void|*}
     */
    loadFilms (filmUrls) {
        let promises = []
        for (let url of filmUrls.values()) {
            if (!this.props.films[getFilmId(url)]) {
                let promise = fetch(url)
                    .then(res => res.json())
                    .catch(err => console.error(err))
                promises.push(promise)
            }
        }

        return Promise.all(promises)
            .then(films => {
                films.forEach(film => {
                    this.props.addFilm(getFilmId(film.url), film)
                })
            })
    }

    handlePageClick (data)  {
        //console.log('handlePageClick', data)
        let current = data.selected

        this.setState({current}, () => {
            this.loadPlanets()
        })
    }

    handleOnSearch (e) {
        let search = e.target.value
        //console.log("handleOnSearch", search)

        if (!search.match(/^([a-zA-Z]+)*$/)) {
            alert('Please use only alpha numerical characters')
            return;
        }

        this.setState({search: search}, () => {
            this.loadPlanets()
        })

    }

    render() {
        return (
            <div className="planets col-md-12">
                <div className="row">
                    <div className="col-lg-4">
                        <div className="form-group row mb-2">
                            <label htmlFor="search" className="col-form-label col-lg-4">Search</label>
                            <div className="col-lg-8">
                                <input id="search" type="text" className="form-control" value={this.state.search} onChange={this.handleOnSearch}/>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-8">
                        <Paginate
                            containerClassName="pagination pull-right-desktop align-center-mobile"
                            pageCount={this.state.pageCount}
                            onPageChange={this.handlePageClick}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <SortedTable
                            data={this.state.items}
                            keyGetter={this.keyGetter}
                            adapter={this.tableAdapter}
                        />
                    </div>
                </div>
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        addFilm: (id, film) => dispatch(addFilm(id, film)),
    }
}

const mapStateToProps = state => {
    return {
        films: state.films
    }
}


Planets.propTypes = {
    perPage: PropTypes.number,
}

Planets.defaultProps = {
    perPage: 10
}

export default connect(mapStateToProps, mapDispatchToProps)(Planets)
