import {combineReducers} from 'redux'
import films from '../components/Film/reducer'

const rootReducer = combineReducers({
    films
})

export default rootReducer