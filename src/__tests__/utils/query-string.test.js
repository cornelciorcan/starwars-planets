import {objectToQueryString} from '../../utils/index'


describe('objectToQueryString', () => {

    it('should return a_param=a_value&b_param=b_value', () => {
        return expect(
            objectToQueryString({
                a_param: 'a_value', 
                b_param: 'b_value'
            })
        ).toBe('a_param=a_value&b_param=b_value')
    })

    it('should return a_param=a_value&b_param=b_value', () => {
        return expect(
            objectToQueryString({
                a_param: 'a_value', 
                b_param: 'b_value'
            })
        ).toBe('a_param=a_value&b_param=b_value')
    })

    it('should throw Error if values are objects', () => {
        let arrayToQueryString = () => objectToQueryString({
            a_param: ['a_1', 'a_2']
        })
        return expect(
            arrayToQueryString
        ).toThrowError()
    })

    it('should return param=null', () => {
        return expect(
            objectToQueryString({
                param: null
            })
        ).toBe('param=null')
    })

    it('should return param=undefined', () => {
        return expect(
            objectToQueryString({
                param: undefined
            })
        ).toBe('param=undefined')
    })
})
