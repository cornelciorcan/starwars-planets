import storeFactory from '../../Store'
import {addFilm} from '../../components/Film/actions'

describe('add film', () => {
    let store

    beforeAll(() => {
        store = storeFactory()

        store.dispatch(addFilm(1, {
            'title': 'A New Hope',
            'episode_id': 4,
            'url': 'https://swapi.co/api/films/1/'
        }))
    })

    it('should add a film to the store', () => {
        expect(Object.keys(store.getState().films).length).toBe(1)
    })

    it('should contain episode 4', () => {
        expect(store.getState().films[1].episode_id).toBe(4)
    })
})

describe('add 2 films', () => {
    let store

    beforeAll(() => {
        store = storeFactory()

        store.dispatch(addFilm(1, {
            'title': 'A New Hope',
            'episode_id': 4,
            'url': 'https://swapi.co/api/films/1/'
        }))

        store.dispatch(addFilm(2, {
            'title': 'The Empire Strikes Back',
            'episode_id': 5,
            'url': 'https://swapi.co/api/films/2/'
        }))
    })

    it('should add 2 films to the store', () => {
        expect(Object.keys(store.getState().films).length).toBe(2)
    })

})