import {configure} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

configure({adapter: new Adapter()})

import React from 'react'
import {shallow} from 'enzyme'
import SortedTable, {ORDER_NONE, ORDER_ASC, ORDER_DESC} from '../../../components/Table/sorted'
import TableAdapter from '../../../components/Table/adapter'
import {mockResponse} from '../../../utils/mock'


describe('<SortedTable /> handleChangeOrder', () => {
    let instance
    beforeAll(() => {
        let wrapper = shallow(<SortedTable data={[]} adapter={new TableAdapter()} keyGetter={jest.fn()}/>)
    	instance = wrapper.instance()
    	instance.setState = jest.fn()
    })

    it('should set new column sorting to "name" and ORDER_ASC', () => {
    	instance.state = {
    		sortBy: null, 
    		sortOrder: ORDER_NONE
    	}
        instance.handleChangeOrder ('name')
        return expect(
            instance.setState
        ).toBeCalledWith({
        	sortBy: 'name', 
    		sortOrder: ORDER_ASC
        })
    })

    it('should set same column from ORDER_ASC to ORDER_DESC', () => {
        instance.state = {
            sortBy: 'name', 
            sortOrder: ORDER_ASC
        }
        instance.handleChangeOrder ('name')
        return expect(
            instance.setState
        ).toBeCalledWith({
            sortBy: 'name', 
            sortOrder: ORDER_DESC
        })
    })

    it('should reset order', () => {
        instance.state = {
            sortBy: 'name', 
            sortOrder: ORDER_DESC
        }
        instance.handleChangeOrder ('name')
        return expect(
            instance.setState
        ).toBeCalledWith({
            sortBy: null, 
            sortOrder: ORDER_NONE
        })
    })

})