import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import storeFactory from './Store'
import 'bootstrap/dist/css/bootstrap.css'
//import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'

ReactDOM.render(
    <Provider store={storeFactory()}>
        <App />
    </Provider>
    , document.getElementById('root')
)

registerServiceWorker()
