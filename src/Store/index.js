import {createStore} from 'redux'
import rootReducer from '../reducers'

const storeFactory = () => createStore(rootReducer)

export default storeFactory